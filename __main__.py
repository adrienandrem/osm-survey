#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 19:17:06 2019

@author: aandre
"""
import os
import logging

import sys

import json
import requests

from osms import HEADERS, Control, controls_to_geojson

import csv

import argparse

import attr

import re

from datetime import datetime

import shapely
from shapely.wkt import loads
from shapely.geometry import box, Point

import random

import configparser

from osms.extra.fr import WayDate, get_ways_date
from osms.extra.fr import get_insee, get_fantoir
from osms.extra.fr import WayAddress, get_ways_addr


logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))


class Area(object):
    """Area of interest"""

    def __init__(self, id, polygon):
        self.id = id
        self.geometry = polygon

    @staticmethod
    def from_js(id, js):
        bounds = js["bounds"]
        minx, miny, maxx, maxy = float(bounds["minlon"]), float(bounds["minlat"]), float(bounds["maxlon"]), float(bounds["maxlat"])

        return Area(id, box(minx, miny, maxx, maxy))


@attr.s(auto_attribs=True)
class Note(object):
    identifier: int

    lon: float = 0.0
    lat: float = 0.0

    timestamp: datetime = None

    text: str = None

    def __lt__(self, other):
        return self.timestamp < other.timestamp

    def url(self):
        return "https://osm.org/note/%s" % self.identifier

    def geometry(self):
        return Point(self.lon, self.lat)

    @staticmethod
    def from_json(json):
        lon, lat = json["geometry"]["coordinates"]
        timestamp_str = json["properties"]["date_created"]

        timestamp = datetime.strptime(timestamp_str, "%Y-%m-%d %H:%M:%S %Z")

        return Note(json["properties"]["id"], lon, lat, timestamp, json["properties"]["comments"][0]["text"])

    def to_control(self):
        return Control(self.lon, self.lat, self.text, "note", self.identifier, self.url())


@attr.s(auto_attribs=True)
class Tag(object):
    element_type: str
    identifier: int

    lon: float = 0.0
    lat: float = 0.0

    timestamp: str = None

    text: str = None

    def code(self):
        return "%s%s" % (self.element_type[0], self.identifier)

    def __lt__(self, other):
        return self.timestamp < other.timestamp

    def __eq__(self, other):
        return self.element_type == other.element_type and self.identifier == other.identifier

    def url(self):
        return "https://osm.org/%s/%s" % (self.element_type, self.identifier)

    def geometry(self):
        return Point(self.lon, self.lat)

    @staticmethod
    def from_list(cells):
        element_type, identifier_str, lon_str, lat_str, timestamp_str, text = cells

        identifier = int(identifier_str)

        lon = float(lon_str if lon_str else 0)
        lat = float(lat_str if lat_str else 0)
        timestamp = timestamp_str

        return Tag(element_type, identifier, lon, lat, timestamp, text)

    def to_control(self, category="tag"):
        return Control(self.lon, self.lat, self.text, category, self.code(), self.url())


@attr.s(auto_attribs=True)
class Error(object):
    identifier: int

    err_class: int = 0
    item: str = 0

    lon: float = 0.0
    lat: float = 0.0

    title: str = None
    subtitle: str = None

    level: int = 0

    def geometry(self):
        return Point(self.lon, self.lat)

    def url(self):
        # return "https://osmose.openstreetmap.fr/fr/api/0.2/error/%s" % self.identifier
        return "https://osmose.openstreetmap.fr/fr/map/#zoom=18&lon=%s&lat=%s&level=%s&item=xxxx" % (self.lon, self.lat, self.level)

    @staticmethod
    def from_list(js_list):
        lat_str, lon_str, id_str, item_str, source_str, class_str, __, __, subtitle, title, level_str = js_list[:11]

        identifier = int(id_str)
        err_class = int(class_str)
        item = int(item_str)

        level = int(level_str)

        lon = float(lon_str if lon_str else 0)
        lat = float(lat_str if lat_str else 0)

        return Error(identifier, err_class, item, lon, lat, title, subtitle, level)

    def to_control(self):
        return Control(self.lon, self.lat, self.title, "error%s" % self.level, self.identifier, self.url())


def aoi_valid_format(aoi):
    """Check input AoI format"""

    return True  # re.match(aoi, r"^r[0-9]+$")


def get_aoi_wkt(id):
    url = "http://polygons.openstreetmap.fr/get_wkt.py?id=%s&params=0.000010-0.000050-0.000100" % id
    logging.debug("AoI URL: %s", url)

    response = requests.get(url, headers=HEADERS)
    wkt = re.sub(r"^SRID=4326;(.+)", r"\1", response.text)

    return Area(id, loads(wkt))


def get_aoi_js(id):
    url = "http://overpass-api.de/api/interpreter?data=[out:json]; relation(%s); out geom;" % id
    logging.debug("AoI URL: %s", url)

    response = requests.get(url, headers=HEADERS)
    data = json.loads(response.text)

    return Area.from_js(id, data["elements"][0])


def get_aoi(aoi):
    """Check input AoI type"""
    identifier = int(aoi[1:])

    area = get_aoi_wkt(identifier)
    # area = get_aoi_js(identifier)

    return area


def geo_filter(controls, aoi):
    # FIXME: Use actual geometry instead of BBox
    return [control for control in controls if control.geometry().intersects(aoi.geometry)]


def get_notes(aoi):
    """Retrieve OSM notes"""
    left, bottom, right, top = aoi.geometry.bounds
    url = "https://api.openstreetmap.org/api/0.6/notes.json?closed=0&bbox=%s,%s,%s,%s" % (left, bottom, right, top)
    # logging.debug("Retrieving notes %s", url)

    response = requests.get(url, headers=HEADERS)
    data = json.loads(response.text)

#    with open("notes.json", "r") as read_file:
#        data = json.load(read_file)

    notes = [Note.from_json(jso) for jso in data["features"]]

    # Keep only those in AoI
    return geo_filter(notes, aoi)


def get_tags(name, aoi):
    """Retrieve location of OSM elements with a fixme tag"""
    left, bottom, right, top = aoi.geometry.bounds
    url = "https://overpass-api.de/api/interpreter?data=[out:csv(::type, ::id, ::lon, ::lat, ::timestamp, %s; true; \";\")][bbox:%s,%s,%s,%s]; nwr[%s]; out center;" % (name, bottom, left, top, right, name)
    logging.debug("Retrieving tags %s", url)

    response = requests.get(url, headers=HEADERS)
    text_data = response.text.splitlines()
    # logging.debug("Overpass data %s", text_data)

    reader = csv.reader(text_data, delimiter=";")

    tags = [Tag.from_list(line) for line in list(reader)[1:]]

    # Keep only those in AoI
    return geo_filter(tags, aoi)


def get_errors(aoi, level):
    left, bottom, right, top = aoi.geometry.bounds
    url = "https://osmose.openstreetmap.fr/fr/api/0.2/errors?full=true&level=%s&bbox=%s,%s,%s,%s" % (level, left, bottom, right, top)
    logging.debug("Retrieving errors %s", url)

    response = requests.get(url, headers=HEADERS)
    data = json.loads(response.text)

    errors = [Error.from_list(jso) for jso in data["errors"]]

    # Keep only those in AoI
    return geo_filter(errors, aoi)


def get_excluded(configfile):
    """Get elements to ignore"""
    excluded = {}

    EXCLUDES = configparser.ConfigParser()
    EXCLUDES.read(configfile)

    if "note" in EXCLUDES:
        note = EXCLUDES["note"]
        if "excluded" in note:
            exclusions = note["excluded"].strip()
            if exclusions:
                excluded["note"] = set([int(nid) for nid in exclusions.split("\n")])

    if "tag_fixme" in EXCLUDES:
        tag_fixme = EXCLUDES["tag_fixme"]
        if "excluded" in tag_fixme:
            exclusions = tag_fixme["excluded"]
            excluded["tag_fixme"] = set(exclusions.split("\n"))

    if "error_l1" in EXCLUDES:
        error_l1 = EXCLUDES["error_l1"]
        if "excluded" in error_l1:
            exclusions = error_l1["excluded"].strip()
            if exclusions:
                excluded["error_l1"] = set([int(eid) for eid in exclusions.split("\n")])

    if "error_l2" in EXCLUDES:
        error_l2 = EXCLUDES["error_l2"]
        if "excluded" in error_l2:
            exclusions = error_l2["excluded"].strip()
            if exclusions:
                excluded["error_l2"] = set([int(eid) for eid in exclusions.split("\n")])

    return excluded


def head(alist, n, shuffle=False):

    if shuffle:
        indices = list(range(min(len(alist), 2*n)))
        random.shuffle(indices)
        indices = indices[:n]
        indices.sort()

        return [alist[i] for i in indices]
    else:
        return alist[:n]


def get_controls(aoi, limit, excluded, shuffle=False):

    controls = []

    # Notes
    notes = get_notes(aoi)
    # FIXMEs
    fixmes = get_tags("fixme", aoi)
    # Osmose errors - Level 1
    errors1 = get_errors(aoi, 1)
    # Osmose errors - Level 2
    errors2 = get_errors(aoi, 2)

    # Filter according to exclusion list
    if "note" in excluded:
        notes = [note for note in notes if not note.identifier in excluded["note"]]
    if "tag_fixme" in excluded:
        fixmes = [fixme for fixme in fixmes if not fixme.code() in excluded["tag_fixme"]]
    if "error_l1" in excluded:
        errors1 = [error for error in errors1 if not error.identifier in excluded["error_l1"]]
    if "error_l2" in excluded:
        errors2 = [error for error in errors2 if not error.identifier in excluded["error_l2"]]

    # Keep top n elements only (optionally shuffle list head a bit)
    notes = head(notes, limit, shuffle)
    fixmes = head(fixmes, limit, shuffle)
    errors1 = head(errors1, limit, shuffle)
    errors2 = head(errors2, limit, shuffle)

    controls = controls + [note.to_control() for note in notes]
    controls = controls + [fixme.to_control("tag_fixme") for fixme in fixmes]
    controls = controls + [error.to_control() for error in errors1]
    controls = controls + [error.to_control() for error in errors2]

    return controls


def get_controls_extra(aoi, limit, excluded, shuffle=False):

    controls = []
    insee = get_insee(aoi)

    ways_dated = get_ways_date(insee)
    fantoir_data = get_fantoir(insee)
    ways_addr = get_ways_addr(fantoir_data)

    # Filter according to exclusion list
    if "way_date" in excluded:
        ways_dated = [way for way in ways_dated if not way.code in excluded["way_date"]]
    if "way_addr" in excluded:
        ways_addr = [way for way in ways_addr if not way.code in excluded["way_addr"]]

    ways_dated.sort()
    ways_addr.sort(reverse=True)

    ways_dated = head(ways_dated, limit, shuffle)
    ways_addr = head(ways_addr, limit, shuffle)

    controls = controls + [way.to_control() for way in ways_dated]
    controls = controls + [way.to_control() for way in ways_addr]

    return controls


if __name__ == "__main__":
    # execute only if run as a script

    PARSER = argparse.ArgumentParser(
            description="Survey roundtrip.",
            prog="survey")
    PARSER.add_argument("AoI",
                            help="Area of Interest (admin zone). Ex.: r1663798")
    PARSER.add_argument("-l", "--limit", type=int, default=10,
                            help="By category control number limit")
    PARSER.add_argument("-s", "--shuffle", action="store_true",
                            help="Shuffle first elements")
    PARSER.add_argument("-x", "--extra", action="store_true",
                            help="Add extra controls")
    ARGS = PARSER.parse_args()

    # Check input data
    if not aoi_valid_format(ARGS.AoI):
        logging.error("Wrong AoI format: %s", ARGS.AoI)
        sys.exit(1)
    if ARGS.limit < 1:
        logging.error("Wrong limit: %s", ARGS.limit)
        sys.exit(2)

    AOI = get_aoi(ARGS.AoI)
    limit = ARGS.limit
    shuffle = ARGS.shuffle
    extra = ARGS.extra

    excluded = get_excluded("exclude.cfg")

    controls = get_controls(AOI, limit, excluded, shuffle)
    if extra:
        controls = controls + get_controls_extra(AOI, limit, excluded, shuffle)
    logging.debug("Controls: %s" % controls)

    if controls:
        # Serialize feature ranking
        for cid, control in enumerate(controls):
            control.fid = cid
        controls_js = controls_to_geojson(controls)
        with open("controls.geojson", "w") as write_file:
            json.dump(controls_js, write_file)
