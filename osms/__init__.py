# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 13:18:47 2020

@author: aandre
"""

import attr

HEADERS = {"User-Agent": "OSMQA Survey/0.1"}


@attr.s(auto_attribs=True)
class Control(object):
    lon: float
    lat: float

    name: str

    ctrl_type: str

    identifier: str

    url: str

    fid: int = None

    def to_geojson(self):
        return {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [self.lon, self.lat]
          },
          "properties": {
            "fid": self.fid,
            "name": self.name,
            "type": self.ctrl_type,
            "identifier": self.identifier,
            "url": self.url
          }
        }

    @staticmethod
    def from_json(jso):
        lon, lat = jso["geometry"]["coordinates"]
        p = jso["properties"]

        return Control(lon, lat, p["name"], p["type"], p["identifier"], p["url"], p["fid"])


def controls_to_geojson(controls):
    return {
        "type": "FeatureCollection",
        "features": [control.to_geojson() for control in controls]
        }
