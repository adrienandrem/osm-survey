# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 13:18:47 2020

@author: aandre
"""

import attr

import re

from .. import Control

import logging
import json
import requests

from .. import HEADERS

TOLERANCE = 0.0001

PATTERN_LOT = "^LOT\s"
PATTERN_CIT = "^CITE\s"
PATTERN_RES = "^RES\s"


def get_insee(aoi):
    """Code INSEE"""
    url = "http://overpass-api.de/api/interpreter?data=[out:json]; relation(%s); out meta;" % aoi.id
    logging.debug("Overpass URL: %s", url)

    response = requests.get(url, headers=HEADERS)
    data = json.loads(response.text)

    code = data["elements"][0]["tags"]["ref:INSEE"]

    return int(code)


def dept(insee):
    """Département"""

    return int(str(insee)[:3])


@attr.s(auto_attribs=True, order=True)
class WayDate(object):
    departement: int
    insee: int
    commune: str

    jour: str = attr.ib(order=True)

    code: str
    nom: str

    lon: float = attr.ib(order=False)
    lat: float = attr.ib(order=False)

    def is_lotissement(self):
        return re.match("^Lotissement\s", self.nom)

    def is_residence(self):
        return re.match("^Residence\s", self.nom)

    def url(self):
        return "http://tile.openstreetmap.fr/~cquest/leaflet/bano.html#20/%s/%s" % (self.lat, self.lon)

    def to_control(self):
        return Control(self.lon, self.lat, self.nom, "way_dated", self.code, self.url())

    @staticmethod
    def from_list(js_list):
        departement_str, insee_str, commune, nom, code, lon_str, lat_str, jour_str = js_list

        lon, lat = float(lon_str), float(lat_str)

        departement = int(departement_str)
        insee = int(insee_str)

        return WayDate(departement, insee, commune, jour_str, code, nom, lon, lat)


def get_ways_date(insee):

    url = "https://dev.cadastre.openstreetmap.fr/fantoir/voies_recentes_manquantes_dept.py?dept=%s" % dept(insee)
    logging.debug("Retrieving dated ways %s", url)

    response = requests.get(url, headers=HEADERS)
    jsdata = json.loads(response.text)

    ways = [WayDate.from_list(voie_js) for voie_js in jsdata]
    ways = [way for way in ways if not way.is_residence()]
    ways = [way for way in ways if not way.is_lotissement()]

    return [way for way in ways if way.insee == insee]


def is_geo(o):
    return abs(o.lon) > TOLERANCE and abs(o.lat) > TOLERANCE


def get_fantoir(insee):
    url = "http://dev.cadastre.openstreetmap.fr/fantoir/requete_fantoir.py?insee=%s" % insee
    logging.debug("Retrieving FANTOIR data %s", url)

    response = requests.get(url)

    return json.loads(response.text)


@attr.s(auto_attribs=True, order=True, frozen=True)
class WayAddress(object):
    housenumbers: int

    code: str
    nom: str = None
    nom_osm: str = None

    lon: float = attr.ib(default=0.0, order=False)
    lat: float = attr.ib(default=0.0, order=False)

    jour: str = "1800-01-01"

    statut: int = 0

    def is_doublon(self):
        return re.match(PATTERN_CARD, self.nom)

    def url(self):
        return "http://tile.openstreetmap.fr/~cquest/leaflet/bano.html#20/%s/%s" % (self.lat, self.lon)

    def is_lotissement(self):
        return re.match(PATTERN_LOT, self.nom)

    def is_cite(self):
        return re.match(PATTERN_CIT, self.nom)

    def is_residence(self):
        return re.match(PATTERN_RES, self.nom)

    def to_control(self, category="way"):
        return Control(self.lon, self.lat, self.nom, category, self.code, self.url())

    def to_geojson(self):
        return {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [self.lon, self.lat]
          },
          "properties": {
            "code": self.code,
            "nom": self.nom,
            "nom_osm": self.nom_osm,
            "statut": self.statut,
            "housenumbers": self.housenumbers
          }
        }

    @staticmethod
    def from_list(js_list):
        code, jour_str, nom_raw, nom_osm_raw, lon_str, lat_str, statut_str = js_list[:7]

        nom = nom_raw.strip()
        nom_osm = nom_osm_raw.strip() if nom_osm_raw != "--" else ""

        lon = float(lon_str if lon_str else 0)
        lat = float(lat_str if lat_str else 0)

        statut = int(statut_str)

        housenumbers = 0

        if len(js_list) == 8:
            nums_str = js_list[7]
            housenumbers = int(nums_str)

        return WayAddress(housenumbers, code, nom, nom_osm, lon, lat, jour_str, statut)


def get_ways_addr(fantoir_data):
    ways_js = fantoir_data[1]

    # Voies à adresses non rapprochées
    ways = [WayAddress.from_list(way_js) for way_js in ways_js]

    ways = [way for way in ways if is_geo(way)]
    ways = [way for way in ways if not way.is_residence()]
    ways = [way for way in ways if not way.is_cite()]
    ways = [way for way in ways if not way.is_lotissement()]

    # Statut
    ways = [way for way in ways if way.statut == 0]

    return ways
