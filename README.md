OpenStreetMap survey round trip
===============================

Tool to plan survey trip from control target points ([notes](https://wiki.openstreetmap.org/wiki/Notes), [quality assurance warnings](https://wiki.openstreetmap.org/wiki/Osmose)).

There are several control categories:
- notes
- [fixme tags](https://wiki.openstreetmap.org/wiki/Key:fixme)
- [Osmose items](https://wiki.openstreetmap.org/wiki/Osmose)


# Dependencies

* Python >= 3.6


# Usage

Get control points in administrative area [relation 1234567](https://www.openstreetmap.org/relation/1234567):

    python __main__.py r1234567

The output is saved to the `controls.geojson` file.

`-l n` option limits output controls by category to `n` points (default 10).

`-s` option shuffles output controls (before count limitation).

To exclude unsolvable points, use the `exclude.cfg` file.
Example to exclude notes `1234567` and `2345678` and fixme tags on `node 3456789` and `way 4567890`:

    [note]
    excluded = 1234567
      2345678
    
    [tag_fixme]
    excluded = n3456789
      w4567890
