#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 19:17:06 2019

@author: aandre
"""
import os
import logging

import json
import requests

import csv

import argparse

import attr

import re

from statistics import mean, median

import gpxpy.gpx

from osms import Control


logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))

HEADERS = {"User-Agent": "OSMQA Survey/0.1"}

TOLERANCE = 0.0001

PATTERN_LOT = "^LOT\s"
PATTERN_CIT = "^CITE\s"
PATTERN_RES = "^RES\s"

PATTERN_CARD = ".+\s(NORD|SUD|OUEST|EST)$"


def is_geo(o):
    return abs(o.lon) > TOLERANCE and abs(o.lat) > TOLERANCE


@attr.s(auto_attribs=True)
class Error(object):
    identifier: int

    err_class: int = 0
    item: str = 0

    lon: float = 0.0
    lat: float = 0.0

    title: str = None
    subtitle: str = None

    level: int = 0

    def to_geojson(self):
        return {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [self.lon, self.lat]
          },
          "properties": {
            "identifier": self.identifier,
            "err_class": self.err_class,
            "item": self.item,
            "title": self.title,
            "subtitle": self.subtitle
          }
        }

    def url(self):
        return "http://osmose.openstreetmap.fr/en/error/%s" % self.identifier

    @staticmethod
    def from_list(js_list):
        lat_str, lon_str, id_str, item_str, source_str, class_str, __, __, subtitle, title, level_str = js_list[:11]

        identifier = int(id_str)
        err_class = int(class_str)
        item = int(item_str)

        level = int(level_str)

        lon = float(lon_str if lon_str else 0)
        lat = float(lat_str if lat_str else 0)

        return Error(identifier, err_class, item, lon, lat, title, subtitle, level)

    def to_control(self):
        return Control(self.lon, self.lat, self.title, "error%s" % self.level, self.identifier)


def errors_to_geojson(errors):
    return {
        "type": "FeatureCollection",
        "features": [error.to_geojson() for error in errors]
        }


@attr.s(auto_attribs=True)
class Waypoint(object):
    lon: float
    lat: float

    index: int

    name: str

    def to_geojson(self):
        return {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [self.lon, self.lat]
          },
          "properties": {
            "index": self.index,
            "name": self.name
          }
        }

    @staticmethod
    def from_json(jso):
        lon, lat = jso["location"]

        return Waypoint(lon, lat, jso["waypoint_index"], jso["name"])


def waypoints_to_geojson(waypoints):
    return {
        "type": "FeatureCollection",
        "features": [waypoint.to_geojson() for waypoint in waypoints]
        }


def osrm_url(router, start, controls):
    service, version, profile = "trip", "v1", "driving"

    start_coords = "%s,%s" % start
    coordinates = start_coords + ";" + ";".join(["%s,%s" % (point.lon, point.lat) for point in controls])
    params = "geometries=geojson&overview=simplified"

    return "%s/%s/%s/%s/%s?%s" % (router, service, version, profile, coordinates, params)


def get_trip(router, start, controls):
    url = osrm_url(router, start, controls)
    # logging.debug("URL: %s", url)

    response = requests.get(url, headers=HEADERS)
    # FIXME: Manage HTTP error codes
    data = json.loads(response.text)

#    with open("trip.json", "r") as read_file:
#        data = json.load(read_file)

    overview = data["trips"][0]["geometry"]
    waypoints = [Waypoint.from_json(jso) for jso in data["waypoints"]]

    return overview, waypoints


def save_point(point, filename):

    jso = {
  "type": "FeatureCollection",
  "name": "point",
  "crs": {
    "type": "name",
    "properties": {
      "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
    }
  },
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "Point",
        "coordinates": list(point)
      }
    }
  ]
}

    with open(filename, 'w') as write_file:
        json.dump(jso, write_file)


def save_routing(overview, waypoints, ovr_file, wpt_file):

    save_overview(overview, ovr_file)

    waypoints_js = waypoints_to_geojson(waypoints)
    with open(wpt_file, 'w') as write_file:
        json.dump(waypoints_js, write_file)


def save_overview(overview, filename):

    jso = {
  "type": "FeatureCollection",
  "name": "line",
  "crs": {
    "type": "name",
    "properties": {
      "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
    }
  },
  "features": [
    {
      "type": "Feature",
      "properties": {
        "id": 0
      },
      "geometry": overview
    }
  ]
}

    with open(filename, "w") as write_file:
        json.dump(jso, write_file)


def load_controls(filepath):
    controls = []

    with open(filepath, 'r') as read_file:
        data = json.load(read_file)

        controls = [Control.from_json(jso) for jso in data["features"]]

    return controls


def parse_start(param):

    # TODO:

    return None


def default_start(controls, mean=False):
    positions = [(control.lon, control.lat) for control in controls]
    lons, lats = [p[0] for p in positions], [p[1] for p in positions]

    if mean:
        lon, lat = mean(lons), mean(lats)
    else:
        lon, lat = median(lons), median(lats)

    return round(lon, 4), round(lat, 4)


def gpx_route(ordered_points, filename):
    gpx = gpxpy.gpx.GPX()
    route = gpxpy.gpx.GPXRoute()
    gpx.routes.append(route)

    for point in ordered_points:
        route.points.append(gpxpy.gpx.GPXRoutePoint(point.lat, point.lon))

    with open(filename, 'w') as gpx_file:
        gpx_file.write(gpx.to_xml())


if __name__ == "__main__":
    # execute only if run as a script

    PARSER = argparse.ArgumentParser(
            description="Survey roundtrip.",
            prog="route")
    PARSER.add_argument("controls",
                            help="Controls")
    PARSER.add_argument("-s", "--start",
                            help="Start position")
    PARSER.add_argument("-r", "--router",
                            help="Routing service",  default="http://router.project-osrm.org")
    ARGS = PARSER.parse_args()

    CONTROLS = ARGS.controls
    START = ARGS.start
    ROUTER = ARGS.router

#    # Macouria
#    insee = 97305
#    start = (-52.4050, 4.9344)
#
#    # Montsinery-Tonnegrande
#    insee = 97313
#    start = (-52.4915, 4.8220)

    # TODO: Get building=*, name=* (overpass-turbo.eu/s/tuT)
    controls = load_controls(CONTROLS)
    # logging.debug("Controls: %s", controls)

    start = parse_start(START)
    if not START:
        start = default_start(controls)
    # logging.debug("Start: %s", start)
    save_point(start, "trip-start.geojson")

    overview, waypoints = get_trip(ROUTER, start, controls)
    logging.debug("Trip: %s, %s", overview, waypoints)
    save_routing(overview, waypoints, "trip-overview.geojson", "trip-waypoints.geojson")

    gpx_route(waypoints, "trip.gpx")

    # TODO: Write GPX route https://www.topografix.com/gpx_manual.asp
